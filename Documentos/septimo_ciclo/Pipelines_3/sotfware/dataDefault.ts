import { App } from './smartphone'

export class AppDefault extends App
{
    init ()
    {
        if (this.isOnApp)
        {
            console.log('App ya esta iniciadad')
        }

        else
        {
            this.isOnApp = true
            console.log('App iniciada con exito!')
        }
    }

    stop ()
    {

        if (!this.isOnApp)
        {
            console.log("app ya esta Apagado")
        }

        else
        {
            this.isOnApp = false
            console.log("Apagado con Exito!")
        }  
        
    }


    category: string

    constructor (_name: string, _releaseDate: Date, _timeUsed: number, _platform: string, _category: string)
    {
        super(_name, _releaseDate, _timeUsed, _platform)
        this.category = _category
    }
}


let defaulapp1: AppDefault = new AppDefault("Deezer", new Date(23), 3, "play store", "music")


defaulapp1.init()
defaulapp1.stop()
console.log(defaulapp1)
