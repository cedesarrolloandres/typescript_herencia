// Preguntarle al profe, en realidad donde se inicia la App, por que en el documento en smartphone, el pide que los metodos a abrir y cerrar app, pero a la vez pide Iniciar y Cerrar App, en la clase Applicaciones.

// clase smartphone independiente
export class Smartphone 
{
    model?: string;

    releaseDay?: number;

    so?: string;

    // installedApp: Map<string, App>

    // appstared: Map<string, App> = new Map([
    //     ["spotify", new AppMusic("spotify2", new Date(), 3, "play store")]
    //     // ["Instagram", new AppPhoto("Instagram", new Date(32), 3, "play store")],
    //     // ["Gmail", new AppMessage("Gmail", new Date(2), 3, "play store")]
    // ])

    AppInstalled: any[] = [];   
    
    InstanceApp: any[] = [];

    constructor (_model: string, _releaseDay: number, _so: string)
    {
        this.model = _model
        this.releaseDay = _releaseDay
        this.so = _so
    }

    // (IsOn) es para definir el estado de Encendido o Apagado
    isOn: boolean = false;

    // Metodo para Encender el Telefono
    turnOn ()
    {
        if (this.isOn)
        {
            console.log("El Smartphone ya esta encedido")
            return "El Smartphone ya esta encedido"
        }

        else
        {
            this.isOn = true
            console.log("Encendido con exito!")
            return "Encendido con exito!"
        }   
    }

    // Metodo para apagar el Telefono
    turnOff ()
    {
        if (!this.isOn)
        {
            console.log("El Smartphone ya esta Apagado")
            return "El Smartphone ya esta Apagado"
        }

        else
        {
            this.isOn = false
            console.log("Apagado con Exito!")
            return "Apagado con Exito!"
        }   
    }

    // Para agregar una Appa a la lista
    downloadApp (typeApp: string, appName: string, status?: boolean)
    {

        
        if (typeApp == "music")
        {

           let AppSong = [typeApp, appName, status = false]
            this.AppInstalled.push(AppSong)

            return "Instala con exito!"
            // let SubMusic: AppMusic = new AppMusic(nameApp, releaseDateApp, timeUsedApp, plataform)

            // console.log(SubMusic)

            // this.installedApp.set(music, SubMusic)
        }

        else if (typeApp == "photo")
        {
            let AppPhoto = [typeApp, appName, status = false]
            this.AppInstalled.push(AppPhoto)

            return true
        }

        else if (typeApp == "message")
        {
            let AppMessage_ = [typeApp, appName, status = false]
            this.AppInstalled.push(AppMessage_)
        
            return true
        }

        // else if (typeApp == 2)
        // {
        //     let SubPhoto: AppPhoto = new AppPhoto(nameApp, releaseDateApp, timeUsedApp, plataform)

        //     console.log(SubPhoto)

        //     this.installedApp.push(SubPhoto)
        // }

        // else if (typeApp == 3)
        // {
        //     let SubMessage: AppMessage = new AppMessage(nameApp, releaseDateApp, timeUsedApp, plataform)

        //     // console.log(SubMessage)

        //     this.installedApp.push.call({SubMessage})
        // }

        else
        {
            throw Error("Tipo de App no existente - music, photo y message")
               
        }

    }


    // Metodo para Abrir App
    openApp (nameApp: string)
    {

        for (let i of this.AppInstalled)
            if (i.includes(nameApp))
            {
           
                this.InstanceApp.push(nameApp)
                console.log("Abierta con exito!")
                return "Abierta con exito!"

            }

    }

    // Metodo para Cerrar App
    closeApp (nameApp: string)
    {
        for (let i of this.AppInstalled)
            if (i.includes(nameApp))
            {
                let posicion = this.InstanceApp.indexOf(nameApp)
                this.InstanceApp.splice(posicion, 1)
                console.log('Eliminada con exito!')
                return "Eliminada con exito!"
            }
    }

}


//--------------------------------------------------------
// Esta clase es abastract y es independiente de smartphone
export abstract class App
{
    name: string;

    releaseDate: Date;

    timeUsed: number;

    platform: string;

    public isOnApp = false


    constructor (_name: string, _releaseDate: Date, _timeUsed: number, _platform: string)
    {
        this.name = _name
        this.releaseDate = _releaseDate
        this.timeUsed = _timeUsed
        this.platform = _platform
    }

    
    // Este metodo Inicia la App
    abstract init (): any


    // Este metodo Detiene la App
    abstract stop (): any


}


export class AppDefault extends App
{
    init ()
    {
        if (this.isOnApp)
        {
            console.log('App ya esta iniciadad')
        }

        else
        {
            this.isOnApp = true
            console.log('App iniciada con exito!')
        }
    }

    stop ()
    {

        if (!this.isOnApp)
        {
            console.log("app ya esta Apagado")
        }

        else
        {
            this.isOnApp = false
            console.log("Apagado con Exito!")
        }  
        
    }


    category: string

    constructor (_name: string, _releaseDate: Date, _timeUsed: number, _platform: string, _category: string)
    {
        super(_name, _releaseDate, _timeUsed, _platform)
        this.category = _category
    }
}


// Clase AppMusic que dependen de la clase App
export class AppMusic extends App
{
    
    init() 
    {
        console.log('AppMusic Iniciada')
        this.isOnApp = true
    }

    stop() 
    {
        console.log('AppMusic Detenida')
        this.isOnApp = false
    }

    // Banco de canciones en la App
    songBank: string[] = ["Dakiti", "Yonagui", "Aloha", "Toda"];

    // Lista de reproducion
    playList: string[] = [];

    // Metodo para buscar una cancion
    searchMusic (songName: string)
    {
        if (this.songBank.includes(songName))
        {
            console.log(`Si existe la cancion ${songName}`)
            return true
        }
        else
        {
            console.log(`La cancion ${songName} no existe`)
            return false
        }
    }

    // Metodo para reproducir una cancion
    playMusic ()
    {
        // Toma una cancion a alzar del banco de Canciones
        let posicion = Math.floor(Math.random() * this.songBank.length)
        console.log(`Estas escuchando: ${this.songBank[posicion]}`)

        return true
    }

    // Añadir a la lista de Reproducion
    songList (songName: string)
    {
        if (this.playList.includes(songName))
        {
            console.log(`${songName} - ya esta en la lista de reproducion`)
            return false
        }
        else
        {
            this.playList.push(songName)
            console.log(`${songName} - fue agregada con exito!`)
            return true
        }
    }

    // Eliminar de la lista de Reproduccion
    deleteSong (songName: string)
    {
        if (this.playList.includes(songName))
        {
            let posicion = this.playList.indexOf(songName)
            this.playList.splice(posicion, 1)

            console.log(`${songName} - fue eliminda de la Playlist con Exito!`)

            return "Eliminada de la lista de reproduccion"

        }

        else
        {
            console.log(`${songName} no esta en la lista de reproducion`)
            
            return "Esta Cancion no existe!"
        
        }
    }
}


// // Clase AppPhoto que dependen de la clase App
export class AppPhoto extends App
{
    init() 
    {
        console.log('AppPhono Iniciada')
        this.isOnApp = true
        return true
    }

    stop() 
    {
        console.log('AppPhoto Detenida')
        this.isOnApp = false
    }

    // Estado de la camara frontal
    statusCameraFrontal = false

    // Estado para saber si la galeria esta abierta
    statusGallery = false

    // Para daber si la App camara esta abierta
    AppCamera = false

    // Metodo para abrir la AppPhoto
    openCamera ()
    {
        console.log('Abriendo Camara')
        this.AppCamera = true

        return true
    }

    // Metodo para la camara frotal
    frontalCamera ()
    {

        if (this.AppCamera)
        {
            if (this.statusCameraFrontal)
            {
                console.log("La frontal ya esta activada!")
                return "La camara frontal ya esta activada"
            }
    
            else
            {
                this.statusCameraFrontal = true
                console.log("Camara frontal Activada con Exito!")
                return "Camara frontal Activada con Exito!"
            }
        }

        else
        {
            console.log("Abra primero la camara")
            return "Abra primero la camara"
        }

    }

    // Metodo para la camara posterior
    rearCamera ()
    {
        if (this.AppCamera)
        {
            if (!this.statusCameraFrontal)
            {
                console.log("La posterior ya esta activada!")
                return "La posterior ya esta activada!"
            }

            else
            {
                this.statusCameraFrontal = false
                console.log("Camara posterior Activada con Exito!")
                return "Camara posterior Activada con Exito!"
            }
        }

        else
        {
            console.log("Abra la camara primero")
            return "Abra la camara primero"
        }
    }

    // Metodo para abrir la galeria
    openGallery ()
    {
        if (this.statusGallery)
        {
            console.log("La galeria ya esta Abierta")
            return "La galeria ya esta Abierta"
        }

        else
        {
            this.statusGallery = true
            console.log("Galeria abierta con exito!")
            return "Galeria abierta con exito!"
        }
    }

    // Metodo para cerrar la galeria
    closeGalery ()
    {
        if (!this.statusGallery)
        {
            console.log("La galeria ya esta cerrada")
            return "La galeria ya esta cerrada"
        }

        else
        {
            this.statusGallery = false
            console.log("La galeria fue cerrada con exito!")
            return "La galeria fue cerrada con exito!"
        }
    }
}


// Clase AppMessage que dependen de la clase App
export class AppMessage extends App
{
    init() 
    {
        console.log('AppMessage Iniciada')
        this.isOnApp = true
    }

    stop() 
    {
        console.log('AppMessage Detenida')
        this.isOnApp = false
    }

    // Este Array va a almacenar todas la informacion del mensaje
    dataEmail: any[] = [];

    // Este Metodo va a Agregar un mensaje al Array
    composeEmail (name: string, affair: string, context: string)
    {
        let email: any[] = [name, affair, context]
        this.dataEmail.push(email)
        console.log("Mensaje redactado con exito!")
        return "Mensaje redactado con exito!"
    }

    // Este metodo va Mostrar un mensaje en especifico
    openMessage (posicion: number)
    {
        console.log(this.dataEmail[posicion])
        return true
    }

    // Este metodo es para eliminar un mensaje en espcifico
    deleteMessage (posicion: number)
    {
        this.dataEmail.splice(posicion, 1)
        console.log('Mensaje eliminado con exito!')
        return "Eliminado con exito!"
    }

}


// Este espacio es para instanciar y probar el codigo
/**Smartphone */
let smartphone_fisico: Smartphone = new Smartphone("Nokia", 2000, "Android")


// /**Instalar App en Smartphone*/
// smartphone_fisico.downloadApp("Facebook")
// smartphone_fisico.downloadApp("Youtube")
// smartphone_fisico.downloadApp("Amazon")

// console.log(smartphone_fisico.downloadApp(1, "Netflix", new Date(), 3, "Play store"))

// console.log(smartphone_fisico.installedApp)


smartphone_fisico.downloadApp("music", "Dezzer")
smartphone_fisico.downloadApp("music", "Spotify")
console.log(smartphone_fisico.AppInstalled)

smartphone_fisico.openApp("Spotify")
smartphone_fisico.openApp("Dezzer")

console.log(smartphone_fisico.InstanceApp+ " intanciadas")

smartphone_fisico.closeApp("Dezzer")

//--------------------------------------------
/**AppMusic */
console.log("\n----------------------------App Music---------------------------------\n")
let appsMusic1: AppMusic = new AppMusic("Spotify", new Date(), 3, "Play Store")

// Buscando una cancion
appsMusic1.searchMusic("Toda")

// Reproducir una cancion del Banco de canciones
appsMusic1.playMusic()

// Agregando una cancion a la Playlist
appsMusic1.songList("La caja negra")

// Eliminando una cancion de la Plylist
appsMusic1.deleteSong("La caja negra")



//---------------------------------------------
/**AppPhoto */
console.log("\n----------------------------App Photo---------------------------------\n")
let appPhoto1: AppPhoto = new AppPhoto("Instagram", new Date(223), 1, "App Store")

// Abrir Camara
appPhoto1.openCamera()

// Camara frontal
appPhoto1.frontalCamera()

// Camara Posterior
appPhoto1.rearCamera()

// Abrir Galeria
appPhoto1.openGallery()

// Cerrar Galeria
appPhoto1.closeGalery()


/**AppMessage */
console.log("\n----------------------------App Message---------------------------------\n")
let appMessage1: AppMessage = new AppMessage("Gmail", new Date(12), 3, "Play Store")

// Redactar correo
appMessage1.composeEmail("Andres", "hello world", "hi my friends, I'm Andres, I live in Colombia.")
appMessage1.composeEmail("Fulano", "Miami", "You have won 1 million dollars, send me your credit card to consign it")

// Para mostrar un mensaje en especifico
appMessage1.openMessage(1)

// Para eliminar un mensaje en especifico
appMessage1.deleteMessage(0)
