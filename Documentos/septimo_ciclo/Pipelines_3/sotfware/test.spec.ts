import { Smartphone, AppMusic, AppPhoto, AppMessage } from "./smartphone";


describe("Test de Smarphone", function()
{
    let smartphone: Smartphone = new Smartphone("Samsung", 4, "Android");

    test("Test de la clase Smartpone", function()
    {

        // Que codigo de encendido no se explote
        expect(() => smartphone.turnOn()).not.toThrow()

        // Verificando que el smartphone encienda
        expect(smartphone.turnOn()).toBe("El Smartphone ya esta encedido");

        // Que no explote el metodo de apagar el Smartphone
        expect(() => smartphone.turnOn()).not.toThrow()

        // Smartphone apagado
        expect(smartphone.turnOff()).toBe("Apagado con Exito!")
    
        // Verificando que el metodo Download no explote
        expect(() => smartphone.downloadApp("music", "play go", false)).not.toThrow()

        // Descargando una App de tipo Photo
        expect(smartphone.downloadApp("photo", "Pinteres", false))

        // Descargando una App de tipo Message
        expect(smartphone.downloadApp("message", "Outlook", false))

        // Descarga App
        expect(smartphone.downloadApp("music", "youtube music", false)).toBe("Instala con exito!")
    
        // Descargando una app que nosea de music, photo o message
        expect(() => smartphone.downloadApp("juego", "fire free", false)).toThrow();

        // Metodo de open Appp
        expect(() => smartphone.openApp("play go")).not.toThrow();

        // Intanciar una App del telefono
        expect(smartphone.openApp("youtube music")).toBe("Abierta con exito!")

        // Metodo de cerrar App
        expect(smartphone.closeApp("play go")).toBe("Eliminada con exito!")

    })


    let appMusic: AppMusic = new AppMusic("Music", new Date(), 3, "Itunes")

    test("Test de la Clase Music", function()
    {

        // Verficando que no de error la busqueda de canciones
        expect(() => appMusic.searchMusic("Toda")).not.toThrow()

        // buscancando una cancion que no exite en la base de datos
        expect(appMusic.searchMusic("FML")).toBe(false)

        // buscando una cancion que si existe en al base de datos
        expect(appMusic.searchMusic("Aloha")).toBe(true)

        // Verificando que el metodo de agregar a al play list no falle
        expect(() => appMusic.songList("Faded")).not.toThrow()

        // Agregar a la lista de reproduccion
        expect(appMusic.songList("I'm yours")).toBe(true)

        // Que no se explote la reproduccion aleatoria
        expect(() => appMusic.playMusic()).not.toThrow()
        
        // Reproducion una cancion Aleatoria
        expect(appMusic.playMusic()).toBe(true)

        // Verificando que el metodo de eliminar cancion de la lista de reproduccion no explote
        expect(() => appMusic.deleteSong("Maria")).not.toThrow()
        
        // Eliminando de la lista de reproduccion
        expect(appMusic.deleteSong("Faded")).toBe("Eliminada de la lista de reproduccion")

        // Eliminando una cancin que no exite
        expect(appMusic.deleteSong("Blue")).toBe("Esta Cancion no existe!")
    })


    let appPhono: AppPhoto = new AppPhoto("Retrica", new Date(23), 4, "IOS")
    
    test("Test de la clase Photo", function()
    {
        // Confirmando el init
        expect(appPhono.init()).toBe(true)

        // Intentado acceder a la camara frontal si primero activar la camara
        expect(appPhono.frontalCamera()).toBe("Abra primero la camara")

        // Intenando acceder a la camara posterior sin haber aprierto la camara
        expect(appPhono.rearCamera()).toBe("Abra la camara primero")

        // Activando camara 
        expect(appPhono.openCamera()).toBe(true)

        // Acediendo a la camara Frontal
        expect(appPhono.frontalCamera()).toBe("Camara frontal Activada con Exito!")

        // Acediendo a la camara cuando ya esta activada
        expect(appPhono.frontalCamera()).toBe("La camara frontal ya esta activada")

        // Accediendo a la camara posterior
        expect(appPhono.rearCamera()).toBe("Camara posterior Activada con Exito!")

        // Accediendo a la camara posterior cuando ya esta activada
        expect(appPhono.rearCamera()).toBe("La posterior ya esta activada!")

        // Abriendo la galeria
        expect(appPhono.openGallery()).toBe("Galeria abierta con exito!")

        // Intenatando volver a abrir la galeria cuando ya esta abrierta XD
        expect(appPhono.openGallery()).toBe("La galeria ya esta Abierta")

        // Cerrando la galleria
        expect(appPhono.closeGalery()).toBe("La galeria fue cerrada con exito!")

        // Intenatndo cerrar la Galeri cuando ya esta cerrada
        expect(appPhono.closeGalery()).toBe("La galeria ya esta cerrada")

        // Verficando que los Metodos no Exploten si los vuelven a llamar
        expect(() => appPhono.openCamera()).not.toThrow()
        expect(() => appPhono.openGallery()).not.toThrow()
        expect(() => appPhono.rearCamera()).not.toThrow()
        expect(() => appPhono.closeGalery()).not.toThrow()
    })


    let appMessaje: AppMessage = new AppMessage("Gmail", new Date(23), 5, "IOS")

    test("Test de la clase Mensaje", function(){

        // Verificando que el metodo de redactar un correo no se explote
        expect(() => appMessaje.composeEmail("Anonimo", "The world is bad", "In other country, the people is more happy, that in Colombia"))

        // Redactando un correo
        expect(appMessaje.composeEmail("Fulano", "Hello world", "Hi my name is Fulano")).toBe("Mensaje redactado con exito!")

        // Redactando un correo
        expect(appMessaje.composeEmail("Mario", "Hi my friends", "I like play soccer")).toBe("Mensaje redactado con exito!")


        // Verificando que el metodo buscar mensaje por Posicion no explote
        expect(() => appMessaje.openMessage(0)).not.toThrow()

        // Este Metodo muestra todos los mensaje redactados segun la posicion del vector
        expect(appMessaje.openMessage(0)).toBe(true)

        // verificando que el metodo de borrar mensaje no explote
        expect(() => appMessaje.deleteMessage(1)).not.toThrow()

        // eliminando un mensaje segun su posicion
        expect(appMessaje.deleteMessage(0)).toBe("Eliminado con exito!")

        

    })
})